var log4js = require("log4js");
var request = require('request');
var _ = require('underscore');
var moment = require('moment')
let device = require('./device.js');


log4js.configure({
  appenders: { alarm: { type: "file",maxLogSize: 20971520, filename: "/home/clarapropavapark/scripts/Alarmas/alarm.log" } },
  categories: { default: { appenders: ["alarm"], level: "debug" } }
});

const logger = log4js.getLogger("alarm");



function optionsLogin(){
	return {		
			          'method': 'POST',
				          'url': 'https://37.153.93.72/api/authenticate',
				          'headers': {
			                        'CON': '',
  			                        'Content-Type': 'application/json'
				                      },
				          rejectUnauthorized: false,
				          requestCert: true,
				          agent: false,
				          body: JSON.stringify({"username":"clarapro","password":"Pavapark2025"})
	
	}
	        
}
			
async function getData(idDevice,token,fieldRule,op,valueRule,devInfo){
	
		  logger.debug("GET_DATA ID_DEVICE "+idDevice+"___"+devInfo["idRule"]["id"]+moment().format("MM ddd, YYYY hh:mm:ss a"));	
		  await request(getOptions("GET",'https://37.153.93.72/api/devices/'+idDevice+'/observations',token), function (error, response) {	  
			if (error) throw new Error(error);
			if (response.body.length > 0){
				const data = JSON.parse(response.body);
				lastElement = data[data.length -1];
				let valueObservation = lastElement[fieldRule]
				logger.debug("GET_DATA LAST ELEMENT "+valueObservation);
				result = compareValues(op,valueObservation,valueRule);
				dReturn = getDataByRfIdDevice(devInfo,idDevice,token,result);						
				logger.debug("******LAST ELEMENT*************");						
				}  
			});
}

function getDataByRfIdDevice(devInfo,idDevice,token,result){	
 reference = devInfo["idRule"]["id"];
 logger.debug("F_RULE_BEFORE")
 logger.debug("F_RULE"+devInfo+"idDevice"+idDevice)
 logger.debug("F_RULE_"+'https://37.153.93.72/api/alerts?deviceId.in='+idDevice+'&reference.in='+reference+"____");
 optionsRF = {
  'method': 'GET',
  'url': 'https://37.153.93.72/api/alerts?deviceId.in='+idDevice+'&reference.in='+reference+'&size=400',
  'headers': {
	'Authorization': 'Bearer '+token,
    'Content-Type': 'application/json'
  },
  body: JSON.stringify({}),
  rejectUnauthorized: false,
  requestCert: true,
  agent: false,
};
let dres = request(optionsRF, function (error, response) {	
	return new Promise( ( resolve, reject ) => {		
  if (error) throw new Error(error); 
  let dta = JSON.parse(response.body);
  logger.debug("getDATABYRF "+response.body);
 
  let dt = _.max(dta, function(o) 
            { 
				return new Date(o.createAt).getTime();
            })
logger.debug("_______LAST VALUE READ _________")		
logger.debug("*********DT ACTUAL VALUE READ*************")
logger.debug(dt)		
logger.debug("**********************")		
  
if (result == true){
	logger.debug("__RESULT TRUE__");
	
	if (dta.length == 0){
		logger.debug("__NO DATA LENGTH__");
		sendAlarmServer(token,devInfo);		
	}else{
		 if (dt.state == "CLOSE"){
			logger.debug("__ DT_STATE CLOSE __");
			sendAlarmServer(token,devInfo);
		}
	   else{
		   logger.debug("__  SE HACE UPDATE DEL ALERT STATUS __");
		   updateALERTStatus(dt,token,"OPEN"); 	
		}		
	}	
}else{
	logger.debug("__VALOR DTA__"+dta);
	if (dta.length > 0){
		logger.debug("__INSIDE RESULT FALSE__");
		updateALERTStatus(dt,token,"CLOSE"); 
	}
	
}

  return dt;
  
	})
  
});
	
return 	JSON.parse(dres.body);
	
}

function updateALERTStatus(dReturn,token,st){	
logger.debug(" INICIO UPDATE ALERT STATUS__");
	var request = require('request');
	logger.debug("F_RULE"+JSON.stringify({"createAt":dReturn.createAt,"id":dReturn.id,"reference":dReturn.reference,"state":dReturn.state})+"____");
	logger.debug("____******______");
		
request(getOptionsP('PUT','https://smartcity.pavapark.com/api/alerts',token,JSON.stringify({"createAt":dReturn.createAt,"id":dReturn.id,"reference":dReturn.reference,"state":st})), function (error, response) {
  if (error) throw new Error(error);
  logger.debug(moment().format("MM ddd, YYYY hh:mm:ss a")+"___ SUCCESS_UPDATE ALERT ___"+response.body);
  logger.debug(response.body);
  logger.debug("  FIN UPDATE ALERT STATUS");	
});	

	
}



function sendAlarmServer(token,deviceInfo){
	logger.debug("***INICIO SEND ALARM SERVER***");
	logger.debug(deviceInfo["deviceReference"]);
	logger.debug("*******");  
	logger.debug("***NAME***");
	logger.debug(deviceInfo["deviceName"]);
	logger.debug(deviceInfo);	
	logger.debug("***IDE_RULE****");
	logger.debug(deviceInfo["idRule"]["id"]);
	logger.debug("***FINAL SEND ALARM SERVER**");



request(getOptionsP("POST",'https://smartcity.pavapark.com/api/alerts',token,JSON.stringify({"createAt":deviceInfo["createAt"],"criticalityLevel":deviceInfo["state"],"description":deviceInfo["description"],"deviceId":deviceInfo["deviceId"],"deviceName":deviceInfo["deviceName"],"deviceReference":deviceInfo["deviceReference"],"message":deviceInfo[""],"reference":deviceInfo["idRule"]["id"],"rule":deviceInfo["rule"],"state":"OPEN"})), function (error, response) {
  if (error) throw new Error(error);
  logger.debug("VIEW__RESPONSE__");
  logger.debug(response.body);
});


	
}


function compareValues(op,value1,value2){
	logger.debug("___ COMPARE VALUES ___");
	logger.debug(op);
	logger.debug("VAL OBSERVATION "+value1);
	logger.debug("VAL RULE "+value2);
	logger.debug("OP "+op);
	let cmp = false
	
	if (op == "EQUALS"){
		logger.debug("EQUALS "+op);
		cmp = (parseInt(value1) == parseInt(value2));
		
	}

	if (op == "GREATER_THAN"){
		logger.debug("greatther THAN "+op +"VAL1 "+value1+" VAL2 "+value2);
		logger.debug("_________");
		logger.debug((parseInt(value1) > parseInt(value2)));
		logger.debug("_________");
		cmp = (parseInt(value1) > parseInt(value2));
	}

	if (op == "GREAT_THAN_OR_EQUAL_TO"){
		logger.debug("equal to "+op);
		cmp =  (parseInt(value1) >= parseInt(value2));
	}

	if (op == "LESS_THAN_OR_EQUAL_TO"){
		logger.debug("less than "+op);
		cmp = (parseInt(value1) <= parseInt(value2));
	}
	return cmp;
}



function getOptions(type,url,token){
	return {
		'method': type,
		'url': url,
		'headers': {
			'Authorization': 'Bearer '+token
		},		
		rejectUnauthorized: false,
		requestCert: true,
		agent: false

	};	
}


function getOptionsP(type,url,token,data){
	return 	{
					'method': type,
					'url': url,
					'headers': {
						'Authorization': 'Bearer '+token,
						'Content-Type': 'application/json'
					},					
					body:data,
		                        'insecure': true
			};	
}




function sendAlarmsParsed(){
	

logger.trace("Send Alarms Parsed");

	
request(optionsLogin(), function (error, response) {
  if (error) throw new Error(error);
   console.log("_RESPUESTA_")
   console.log(response.body)
   console.log("____________")	
   let obj = JSON.parse(response.body);
   logger.debug(obj.id_token);
   let token = obj.id_token;


request(getOptions("GET",'https://37.153.93.72/api/rules',token), function (error, response) {
  if (error) throw new Error(error);
  console.log("VALOR TOKEN"+token)
  console.log("SEND_ALARM_PARSED___ "+response.body);
  if (response.body.length > 0){

	  const rules = JSON.parse(response.body);	  
	  _.map(rules, function(rule) { 
          console.log("*****_INSIDE_A_NEW_RULE_********")
          console.log(rule) 
	  console.log("********RULE_DEVICE___LENGTH*****"+rule["devices"].length)
	  if (rule["devices"].length > 0){
		        console.log("____RULE_DEVICES_LENGTH > 0________")
		        console.log(rule["devices"].length)
		        console.log("_______________")
			idDevice  = rule["devices"][0].id;
		        console.log("_1_")
		        console.log(idDevice)
			idRule  = rule["ruleCompareFields"][0];
			console.log("_2_")		  
		        console.log(idRule)
			deviceName = rule["devices"][0].deviceTypeName;
		        console.log("_3_")
		        console.log(deviceName)
			message = rule["description"];
			logger.debug("___ MESSAGE ___");
			logger.debug(message);
			logger.debug("________________");
			description = rule["description"];
			deviceReference = rule["devices"][0].reference;
			rl = rule["devices"][0].reference;
			reference = rule["ruleCompareFields"];
			logger.debug("______________");
			logger.debug(reference);
			logger.debug("______________");
	   	  
			criticalityLevel = rule["levelRule"];
			compareRule = reference[0].compareType;
			fieldRule = reference[0].fieldName;
			valueRule = reference[0].value;
			//description = rule["description"];
			logger.debug("__DEVICE REFERENCE__"+rule["devices"][0].id+" __REFERENCE RULE__"+reference[0].id);
	  
	  
	  state = rule["levelRule"];
	  ruleType = rule["evaluationType"];
	  createAt = moment().toISOString();



	  deviceInfo = new device(idDevice,deviceName,createAt,criticalityLevel,description,deviceReference,message,ruleType,state,idRule);
	  
	  //logger.debug(idDevice+"__"+deviceName+"__"+createAt+"___"+criticalityLevel+"__"+description+"__"+deviceReference+"___"+message+"__"+ruleType+"__"+state)
	  logger.debug("____***********VIEW ALL DEVICE__INFO********_______");
          logger.debug(idDevice+"__"+deviceName+"__"+createAt+"___"+criticalityLevel+"__"+description+"__"+deviceReference+"___"+message+"__"+ruleType+"__"+state)
          logger.debug(deviceInfo);
	  logger.debug("____*******************_______");
	  
	  getData(idDevice,token,fieldRule,compareRule,valueRule,deviceInfo);
	  }




	  logger.debug("*****FIELD RULE 1******");
	  logger.debug(fieldRule);
 
	  logger.debug("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");

	  });
		
  }
 
  
  
});

});	
	
	
}


sendAlarmsParsed();






















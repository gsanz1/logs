class device {
  constructor(deviceId,deviceName,createAt,criticalityLevel,description,deviceReference,message,rule,state,idRule) {
    this.deviceId = deviceId;
    this.deviceName = deviceName;
	this.createAt = createAt;
	this.criticalityLevel = criticalityLevel;
	this.description = description;
	this.deviceReference = deviceReference;
	this.message = message;
	this.rule = rule;
	this.state = state;
	this.idRule = idRule;
  }
    
}
module.exports = device;

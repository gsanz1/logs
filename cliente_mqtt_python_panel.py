# coding=utf-8
import paho.mqtt.client as mqtt
import requests
import json
import datetime
import logging
import logging.handlers
import os.path as path
#import sched    #posible planificador de ejecuciones
#import time     #posible planificador de ejecuciones
#import the socket library 
import socket
import select
import binascii
from datetime import timedelta
from requests.auth import HTTPBasicAuth
from requests.adapters import HTTPAdapter
from requests.exceptions import Timeout
import sys
import time
sys.path.append('../mail/mail.py')

broker_address = "eu.thethings.network"
broker_port = 1883
#topic = "#"
topic = "+/devices/+/up"
INFO_FRAME = "Info Frame"
KEEP_ALIVE_FRAME = "Keep Alive Frame"
RTC_UPDATE_FRAME ="RTC UPDATE Frame"
DEVICE_TYPE_PANEL = "PanelLora"
URL_PANELES = 'source'
TIMEOUT = 5


#serverSmartCity ="localhost"
serverSmartCity ="37.153.93.72"
serverSession = "https://37.153.93.72"
#serverSmartCity ="5.35.200.167"
logFile="/home/clarapropavapark/RaspMote/TTN-LORA-VINATEA.log"

userSmartCity="clarapro"
passwordSmartCity="Pavapark2022"



logging.basicConfig(
    filename=logFile,
    level=logging.DEBUG,
    format='%(asctime)s.%(msecs)03d %(levelname)s %(module)s - %(funcName)s: %(message)s',
    datefmt='%Y-%m-%d %H:%M:%S',
)

#Configure logging options
logging.debug('This message should go to the log file')
logging.info('So should this')
logging.warning('And this, too')
#----------------------------------------------
#configure max_retries on requests.request
s = requests.Session()
s.mount(serverSession, HTTPAdapter(max_retries=5))
#-----------------------------------------------
#configure scheduler
#scheduler = sched.scheduler(time.time, time.sleep)

#def print_event(name):
#    print ('EVENT:', time.time(), name)

#-------------------OBTENER FECHA ACTUAL--------------------
def getActualTime():
    from datetime import datetime
    now = datetime.now()
    t = now.strftime("%d/%m/%Y %H:%M:%S")       
    t.replace("-","/",3)
    return t
    #return  now.strftime("%d/%m/%Y %H:%M:%S")
#-------------------QUITAR EL PRIMER Y ÚLTIMO CARACTER ----------------
def remove_char(s):
    return s[1 : -1]

#------------------SEND REQUEST TO PLATFORM ---------------------------------------
def sendRequestToPlatform (request_order, request_url, request_payload, request_headers, request_timeout):
    try:
        r = requests.request(request_order, request_url, data=json.dumps(request_payload), headers=request_headers, timeout=request_timeout,verify=False)    
    except requests.exceptions.Timeout:
        # Maybe set up for a retry, or continue in a retry loop
        logging.info("TIMEOUT")
        print("TIMEOUT")
        r = "ERROR"
    except requests.exceptions.TooManyRedirects as erru:
        # Tell the user their URL was bad and try a different one
        logging.info(erru)
        print("ERROR EN URL")
        r = "ERROR"
    except requests.exceptions.ConnectionError as errc:
        print ("Error Connecting:",str(errc))
        logging.info(errc)
        r= "ERROR"
    except requests.exceptions.RequestException as e:
        # catastrophic error. bail.
        print("Otro error ",str(e))
        logging.info(e)
        r= "ERROR"
        #raise SystemExit(e)
    return (r)

#-------------------SEND DATA TO PHYSICAL PANEL ----------------------
# dirección a la que enviar, con puerto y plazas a enviar
def sendpanelnotification(url_panel,spotsAvailable):
    # create a socket object 
    sock = socket.socket()          
    print("Socket successfully created")
    sock.setblocking(0)
    sock.settimeout(TIMEOUT)   # 5 seconds
    # connect to the server on local computer, 2404 default port for IEC-104 Server
    url_splited = str(url_panel).split(":")
    print(str(url_splited))
    port = url_splited[int(len(url_splited))-1]
    print(str(port))
    url_splited.remove(port)
    url_final = url_splited[0]
    print(str(url_final))
    print(str(port))
    spots_send_panel = str('{:03d}'.format(int(spotsAvailable)))
    print(spots_send_panel)
    data0_spots_send_panel = str(int(spots_send_panel[0]) + 48)
    data1_spots_send_panel = str(int(spots_send_panel[1]) + 48)
    data2_spots_send_panel = str(int(spots_send_panel[2]) + 48)
    #spots_send_panel = str(data0_spots_send_panel+data1_spots_send_panel+data2_spots_send_panel)
    print(spots_send_panel)
    #tamaño de trama 0A 00 fijado a mano
    print("BEFORE DATA TO SEND")
    print("_1_")
    print(str('{:02x}'.format(int(data0_spots_send_panel))))
    print("_2_")
    print(str('{:02x}'.format(int(data1_spots_send_panel))))
    print("_3_")
    print(str('{:02x}'.format(int(data2_spots_send_panel))))
    print("_4_")
    data_to_send = "160A000127"+str('{:02x}'.format(int(data0_spots_send_panel)))+str('{:02x}'.format(int(data1_spots_send_panel)))+str('{:02x}'.format(int(data2_spots_send_panel)))
    print("__BEFORE HEX DATA__")
    print(data_to_send)
    #hex_data = bytes.fromhex(data_to_send)
    hex_data = bytearray.fromhex(data_to_send)    
    print("_AFTER_HEX_DATA___")
    checksum = 0
    print("__7__")
    for ch in hex_data:
        checksum = checksum + int((ch))
    print("__8__")
        #print(str(checksum))
    #print(str(chr(checksum)))
    cs_send = str('{:04x}'.format(checksum))
    print("__9__")
    #print(cs_send)
    cs_send = cs_send[-2:]+cs_send[:-2]
    print("__10__")
    print(cs_send)
    #data_to_send = data_to_send + str('{:04x}'.format(checksum))
    print("__11__")
    data_to_send = data_to_send + cs_send
    print("____*******______")
    print(str(data_to_send))
    print(str(url_final))
    print(int(port))
    print("____*****_______")
    try:
        sock.connect((str(url_final),int(port)))        
        #sock.send(b'\x16\x0B\x00\x01\x27\x31\x32\x33\x34\x13\x01')
        sock.send(bytes.fromhex(data_to_send))
        # send a hex message in bytes
    except socket.error as exc:
        print ("Caught exception socket.error :"+str(exc))
        logging.info("Caught exception socket.error :"+str(exc))
        #sock.close()
        #print("Socket successfully closed") 
    
    ready = select.select([sock], [], [], TIMEOUT)
    if ready[0]:
        data = sock.recv(1024)
        print( "received data:"+ str(data))
    sock.close()
    print("Socket successfully closed") 
def sendpanelLoraPavapark(port,url_final,spotsAvailable):
	# create a socket object 
	sock = socket.socket()          
	logging.info("Socket successfully created")
	sock.setblocking(0)
	sock.settimeout(TIMEOUT)   # 5 seconds
	spots_send_panel = str('{:04d}'.format(int(spotsAvailable)))
	logging.info(spots_send_panel)
	data0_spots_send_panel = str(int(spots_send_panel[0]) + 48)
	data1_spots_send_panel = str(int(spots_send_panel[1]) + 48)
	data2_spots_send_panel = str(int(spots_send_panel[2]) + 48)
	data3_spots_send_panel = str(int(spots_send_panel[3]) + 48)

#	data_to_send = "160A000127"+str('{:02x}'.format(int(data0_spots_send_panel)))+str('{:02x}'.format(int(data1_spots_send_panel)))+str('{:02x}'.format(int(data2_spots_send_panel)))+str('{:02x}'.format(int(data3_spots_send_panel)))
	data_to_send = "160B000127"+str('{:02x}'.format(int(data0_spots_send_panel)))+str('{:02x}'.format(int(data1_spots_send_panel)))+str('{:02x}'.format(int(data2_spots_send_panel)))+str('{:02x}'.format(int(data3_spots_send_panel)))
	logging.info("__SEND NO HEX DATA__"+data_to_send)
	#print(data_to_send)
	#hex_data = bytes.fromhex(data_to_send)
#	hex_data = bytearray.fromhex(data_to_send)    

#	data_to_send = "160B00012735383734"

	hex_data = bytearray.fromhex(data_to_send)    
	logging.info("_HEX_DATA__")
	logging.info(hex_data)
	checksum = 0
	for ch in hex_data:
		checksum = checksum + int((ch))
	cs_send = str('{:04x}'.format(checksum))
	cs_send = cs_send[-2:]+cs_send[:-2]
	logging.info("__VALOR CHECKSUM__"+cs_send)
	data_to_send = data_to_send + cs_send
	logging.info("DATA_SENT")
	logging.info(data_to_send)
	try:
		print("BEFORE__CONNECT_")
                logging.info("BEFORE_CONNECT_")
		sock.connect((str(url_final),int(port)))
                logging.info("AFTER_CONNECT_BEFORE_SEND")
		print("AFTER__CONNECT")
		#sock.send(b'\x16\x0B\x00\x01\x27\x35\x38\x37\x34\x21\x01')
		#sock.send(b'\x16\x0B\x00\x01\x27\x35\x38\x37\x34\x21\x01')
		#sock.send(bytes.fromhex(data_to_send))
		sock.send(bytearray.fromhex(data_to_send))
                logging.info("AFTER_SEND_")
		# send a hex message in bytes
	except socket.error as exc:
		logging.info("Caught exception socket.error :"+str(exc))
		logging.info("Caught exception socket.error :"+str(exc))
		sock.close()
		logging.info("Socket successfully closed")
                msg = "Error al enviar Plazas al Panel "+str(getActualTime())
                mail.send(msg)
	ready = select.select([sock], [], [], TIMEOUT)
	if ready[0]:
		data = sock.recv(1024)
		logging.info( "received data:"+ str(data))
	sock.close()
	logging.info("Socket successfully closed")
#-------------------SEND DATA PARKING GROUP ---------------------------
#-------------------SEND DATA PARKING GROUP ---------------------------
def sendDataParkingGroup(nPanel,pLibres,estado_plaza,tUpdate,tSend,tok,refresh):
    print("SEND_DATA_PARKING_GROUP")
    print(refresh)

    namefilepanel = 'lastdata'+str(nPanel)+'.json'
    url = 'https://'+serverSmartCity+'/api/devices/'+nPanel+'/data'   
    if(refresh == False):
        print("caso1 FALSE")
        print(estado_plaza)
        print("PLAZAS LIBRES INICIALES "+str(pLibres))
        if(estado_plaza == 1):
            pLibresPanel = (int(pLibres) - 1) #plaza ocupada, notificamos una menos libre
            if(pLibresPanel<0):
                pLibresPanel = 0
        elif(estado_plaza == 0):
            pLibresPanel = (int(pLibres) + 1) #plaza libre, notificamos una más
        payload = {
        "availableSpotNumber": str(pLibresPanel),
        #"lastTimeUpdated":lastTimeUpdated,
        "dateObserved":tSend
        }
        print(payload)
    else: #OJO! Si no existe el fichero y tiene que actualizar, crash de programa!
        print("CASO2")
        if path.exists(namefilepanel):
            # código
            print("Existe el fichero de panel!!\r\n")
            #leemos el último estado almacenado para saber si tenemos que actualizar panel o no
            with open(namefilepanel, 'r') as file:
                lastdata = json.load(file)
                print(lastdata)
                payload = {
                "availableSpotNumber": lastdata['availableSpotNumber'],
                #"lastTimeUpdated":lastTimeUpdated,
                "dateObserved":lastdata['dateObserved']
                }
                pLibresPanel = str(lastdata['availableSpotNumber'])     
    headers = {'Content-Type': 'application/json','Authorization': 'Bearer '+tok}
    logging.info("URL "+str(url)+" PAYLOAD "+str(payload)+ "HEADERS"+str(headers)+"LAST ACTUALIZATION"+tUpdate)
    #print("URL "+str(url)+" PAYLOAD "+str(payload)+ "HEADERS"+str(headers)+"LAST ACTUALIZATION"+tUpdate+"\n")
    print(str(url))
    #requests.request("POST",url, data=json.dumps(payload), headers=headers)
    r = sendRequestToPlatform ("POST", url, payload, headers, TIMEOUT)
    logging.info(str(r.content))
    payload_saved = {
    "name":str(nPanel),
    "availableSpotNumber": str(pLibresPanel),
    #"lastTimeUpdated":lastTimeUpdated,
    "dateObserved":tSend
    }
    print("Nombre del fichero Panel " + namefilepanel)
    with open(namefilepanel, 'w') as file:
        json.dump(payload_saved, file)
        print("Guardado en " +namefilepanel+ "\n")
    print("Plazas libres actuales:"+str(pLibresPanel))
    return pLibresPanel 

#-------------------OBTENER PLAZAS LIBRES PANEL ----------------------
def obtainAvailableSpots(ipanel,tok):
    url = 'https://'+serverSmartCity+'/api/devices/'+str(ipanel)+'/observations?size=1'
    print(url)
    headers = {'Content-Type': 'application/json','Authorization': 'Bearer '+tok}
    #logging.info("URL "+str(url)+"HEADERS"+str(headers)+"LAST ACTUALIZATION"+timeUpdate)
    #print("URL "+str(url)+"HEADERS"+str(headers)+"\n")
    resp = sendRequestToPlatform ("GET", url, None, headers, TIMEOUT)
    #resp = requests.request("GET",url, headers=headers)
    #print(resp.content)
    if(resp != "ERROR"):
        respuesta = remove_char(resp.content)
        respuesta = json.loads(respuesta)
        #print(respuesta)
        plazas_libres= respuesta['availableSpotNumber']
        print("Plazas libres:"+str(plazas_libres))
        return plazas_libres
    else:
        return ("ERROR")


#-------------------OBTENER DISPOSITIVOS PANELES-----------------------
def obtainDevicePanel(itype,time,tok):
    url = 'https://'+serverSmartCity+'/api/devices?deviceTypeId.equals='+str(itype)
    print(url)
    headers = {'Content-Type': 'application/json','Authorization': 'Bearer '+tok}
    #logging.info("URL "+str(url)+"HEADERS"+str(headers)+"LAST ACTUALIZATION"+timeUpdate)
    #print("URL "+str(url)+"HEADERS"+str(headers)+"LAST ACTUALIZATION"+time+"\n")
    #resp = requests.request("GET",url, headers=headers)
    resp = sendRequestToPlatform ("GET", url, None, headers, TIMEOUT)
    #print(resp.content)
    #respuesta = remove_char(resp.content)
    devices = json.loads(resp.content)
    #print(devices)
    return devices


#--------------------ENVIAR DATOS A PANELES ASOCIADOS AL CAMBIO RECIBIDO--------
def modifyParkingGroup(r,estado,timeUpdate,timeSend,token,refresh_panel):
    #esta parte la ponemos pq no coincide el nombre del dispositivo en la plataforma con el nombre del dispositivo en LORAWAN
    if(r=="parkmote02"):
        r="parkmote2"
    elif (r=="parkmote01"):
        r="parkmote1"
    else:
        r= r
	#revisar lo anterior para poner en producción

    url = 'https://'+serverSmartCity+'/api/device-types?reference.equals='+DEVICE_TYPE_PANEL
    headers = {'Content-Type': 'application/json','Authorization': 'Bearer '+token}
    #logging.info("URL "+str(url)+"HEADERS"+str(headers)+"LAST ACTUALIZATION"+timeUpdate)
    #print("URL "+str(url)+"HEADERS"+str(headers)+"LAST ACTUALIZATION"+timeUpdate+"\n")
    #resp = requests.request("GET",url, headers=headers)
    print(url)
    print(headers)
    resp = sendRequestToPlatform ("GET", url, None, headers, TIMEOUT)
    print("_RESPUESTA_")
    print(resp)
    print("_CONTENIDO_")
    print(resp.content)
    print("___________")
    respuesta = remove_char(resp.content)
    logging.info(respuesta)
    print("*******")
    print(respuesta)
    print("*******")
    device_types = json.loads(respuesta)
    print(device_types)
    device_types_id = device_types['id']
    print(device_types_id)
    #consulto los devices del tipo correspondiente a DEVICE_TYPE_PANEL
    paneles = obtainDevicePanel(device_types_id,timeUpdate,token)
    #respuesta = remove_char(paneles)
    sendphysicalpanel = False
    url_paneles = False

    print("_______")
    print(paneles)
    print("_______")

    for dict_item in paneles:
        for key in dict_item:
            print(key)
            print(dict_item[key])
            print(r)
            print("VALOR DE R")
            if(key == 'id'):
                id_panel = dict_item[key]
            if(key == 'reference'):
                namePanel = dict_item[key]
            if(key == 'observaciones'):
                parkingSpot = dict_item[key]
                print(parkingSpot)
                print('Pertenece a este panel')
                sendphysicalpanel = True #debemos enviar a panel físico
                #obtenemos las plazas libres del panel correspondiente con un GET a Clara
                plazaslibres = obtainAvailableSpots(id_panel,token)
                #modifico plazas libres panel en función del dato obtenido del sensor
                print("VALOR PLAZAS LIBRES "+str(plazaslibres))
                spotsAvailableSended = sendDataParkingGroup(namePanel,plazaslibres,estado,timeUpdate,timeSend,token,refresh_panel)
#               if((parkingSpot.find(r)) >= 0 ) :
#                    print('Pertenece a este panel')
#                    sendphysicalpanel = True #debemos enviar a panel físico                    
                    #obtenemos las plazas libres del panel correspondiente con un GET a Clara
#                    plazaslibres = obtainAvailableSpots(id_panel,token)
                    #modifico plazas libres panel en función del dato obtenido del sensor
#                    spotsAvailableSended = sendDataParkingGroup(namePanel,plazaslibres,estado,timeUpdate,timeSend,token,refresh_panel)
            if((key == 'fields') & (sendphysicalpanel)):
                sendphysicalpanel = False
                fields = dict_item[key]
                print("******FILTERED STATUS********")
                #print(fields)
                filteredStatus = list(filter(lambda x: x['name']== 'source', fields))
                print(filteredStatus)
                url_panel = filteredStatus[0]['value']
                print(filteredStatus[0])
                print(url_panel)
                print("SPOTS_AVAILABLE "+str(spotsAvailableSended))
                try:
                   #sendpanelnotification(url_panel,spotsAvailableSended)
                   sendpanelLoraPavapark(3801,"217.125.52.181",spotsAvailableSended)
                except:
                    print("Error inesperado:", sys.exc_info()[0])
                print("**************")
#               for aux_dict_item in fields:
#                   for aux_key in aux_dict_item:
#                       print("----------------")
#                       print(aux_key)
#                       print(aux_dict_item[aux_key])
#                       print("----------------")
                        #print(aux_key)
                        #print (aux_dict_item[aux_key])
#                       if(aux_dict_item[aux_key] == URL_PANELES):
#                           url_paneles = True
#                       if((aux_key == 'value') & (url_paneles)):
#                           url_paneles = False
#                           url_panel = aux_dict_item[aux_key]
#                           print("****************")
#                           print(aux_dict_item)
#                           print(aux_key)
#                           print("URL PANEL FISICO:"+str(url_panel)) 
#                           print("***************")
                            #TO-DO comunico al panel físico el dato
#                           sendpanelnotification(url_panel,spotsAvailableSended) #dirección a la que enviar, con puerto y plazas a enviar



#-------------------ENVIAR DATOS A SMARTCITY--------------------
def modifyParking(r,status,batteryState,sensorrecalibration,lastTimeUpdated,lastTimeSent,token):
    #esta parte la ponemos pq no coincide el nombre del dispositivo en la plataforma con el nombre del dispositivo en LORAWAN
    if(r=="parkmote02"):
        r="parkmote2"
    elif (r=="parkmote01"):
        r="parkmote1"
    else:
        r= r
	#revisar lo anterior para poner en producción

    namefile = 'lastdata'+str(r)+'.json'

    url = 'https://'+serverSmartCity+'/api/devices/'+str(r)+'/data'   
    payload = {
    "status": status,
    "batteryState": batteryState,
    "sensorrecalibration": sensorrecalibration,
    #"lastTimeUpdated":lastTimeUpdated,
    "dateObserved":lastTimeSent
    }
    refresh_status_panel = False
    if path.exists(namefile):
        # código
        print("Existe el fichero!!\r\n")
        #leemos el último estado almacenado para saber si tenemos que actualizar panel o no
        with open(namefile, 'r') as file:
            lastdata = json.load(file)
            #print(lastdata)
            if(lastdata['status']==payload['status']):
                print("No es necesario actualizar paneles")
                #posible refresco de trama de paneles enviada que pertenecen a este nodo...
                refresh_status_panel = True
            else:
                print("Actualizo paneles")
                #Ahora modificamos los paneles correspondientes
                refresh_status_panel = False
            modifyParkingGroup(str(r),payload['status'],lastTimeUpdated,lastTimeSent,token,refresh_status_panel)

    payload_saved = {
    "name": str(r),
    "status": status,
    "batteryState": batteryState,
    "sensorrecalibration": sensorrecalibration,
    "dateObserved":lastTimeSent    
    }    
    headers = {'Content-Type': 'application/json','Authorization': 'Bearer '+token}
    logging.info("URL "+str(url)+" PAYLOAD "+str(payload)+ "HEADERS"+str(headers)+"LAST ACTUALIZATION"+lastTimeUpdated)
    #print("URL "+str(url)+" PAYLOAD "+str(payload)+ "HEADERS"+str(headers)+"LAST ACTUALIZATION"+lastTimeUpdated+"\n")
    print(str(url))
    #r = requests.request("POST",url, data=json.dumps(payload), headers=headers)
    r = sendRequestToPlatform ("POST", url, payload, headers, TIMEOUT)
    
    #print(r.content)
    print("Nombre del fichero " + namefile)
    with open(namefile, 'w') as file: 
        json.dump(payload_saved, file)
        print("Guardado en " +namefile+ "\n")
    logging.info("RESPONSE "+r.content+"\n")




#def obtainToken():
#    url = 'https://'+serverSmartCity+'/api/authenticate'
#    headers = {'Content-Type': 'application/json'}
#    payload = {
#        "username": userSmartCity,
#        "password": passwordSmartCity
#    }
#    r = sendRequestToPlatform ("POST", url, payload, headers, TIMEOUT) 
#    logging.info(str(r.content))
#    if(r != "ERROR"):
#        token = json.loads(r.content)
#        print(token)
#        return token['id_token']
#    else:
#        return (r)



#ef obtainToken():
#   url = 'https://'+serverSmartCity+'/api/authenticate'
#   headers = {'Content-Type': 'application/json'}
#   payload = {
#       "username": userSmartCity,
#       "password": passwordSmartCity
#   }

#   r = requests.request("POST", url, headers=headers, data = payload,verify=False)
#   token = json.loads(r.text)
#   return token['id_token']
    



#-------------------CONECTAR FRENTE A PERDIDAS DE CONEXION--------------------
def on_connect(client, userdata, flags, rc):
 print("Connected with result code " + str(rc))
 print("UserData= " + str(userdata))
 print("flags= " + str(flags))
 print("")
 print("TOPIC SUSCRIBED "+topic)
 client.subscribe(topic)

#-------------------ANALIZAR AL DETECTAR MENSAJE RECIBIDO--------------------
def on_message(client, userdata, message):
 print("\r\n")
 print("Topic=", message.topic)
 print("Mensaje recibido=", str(message.payload.decode("utf-8")))
 logging.info("RECIBIDO "+str(message.payload.decode("utf-8"))+"\n")
 print("Nivel de calidad [0|1|2]=", message.qos)
 print("Flag de retención =", message.retain)
 data = json.loads(str(message.payload.decode("utf-8")))
 #print("DATA JSON:",data)
 dev_id = data['dev_id']
 status =data['payload_fields']['BasicData']['status']
 batteryState = data['payload_fields']['BasicData']['batteryState']
 sensorrecalibration = data['payload_fields']['BasicData']['sensorrecalibration']
 frametype = data['payload_fields']['BasicData']['Name']
 
 lstamp = data['metadata']['time']
 print("FRAME TYPE =",frametype,"NAME=",dev_id,"STATUS=",status,"BatteryState=",batteryState,"Sensorrecalibration=",sensorrecalibration, "TimeStamp=",lstamp)
 if (frametype == INFO_FRAME) or (frametype == KEEP_ALIVE_FRAME): # or (frametype == RTC_UPDATE_FRAME):
    lastTimeSent = getActualTime() 
    #print("Despues de tiempo")
    tokenSmartCityOnda = obtainToken()	
    #print("Despues de token")
    modifyParking(dev_id,status,batteryState,sensorrecalibration,lstamp.replace("-","/",3),lastTimeSent,tokenSmartCityOnda)
    



def obtainToken():   
   url = 'https://'+serverSmartCity+'/api/authenticate'
   payload = "{\r\n   \"username\": \"clarapro\",\r\n   \"password\": \"Pavapark2022\"\r\n}\r\n"
#   payload = {
#       "username": userSmartCity,
#       "password": passwordSmartCity
#   }

   headers = {
      'CON': '',
      'Content-Type': 'application/json'
   }
   r = requests.request("POST", url, headers=headers, data = payload,verify=False)
   token = json.loads(r.text)
   return token['id_token']



#token = obtainToken()
#headers = {'Content-Type': 'application/json','Authorization': 'Bearer '+token}
#url ="https://37.153.93.72/api/devices/parkmote4/data"
#payload = {
#           "availableSpotNumber": "7",
#           "dateObserved":"16"
#          }
#r = sendRequestToPlatform ("POST", url, payload, headers, TIMEOUT)


client = mqtt.Client('Cliente1') 
client.on_connect = on_connect
client.on_message = on_message 
client.username_pw_set(username="poc_parking_sensor_lora_pavapark",password="ttn-account-v2.rDyhQKA-i00ZQrAQJdY104SMdZzSHC39t8TCBUIvlHA")
client.connect(broker_address, broker_port, 60) 
client.subscribe(topic) # Subscripción al topic
client.loop_forever()

#k = 0
#st = 23
#while (k < 11):
#	st = st + 1
#	sendpanelLoraPavapark(3801,"217.125.52.181",st)
#	time.sleep(1)
#sendpanelLoraPavapark(3801,"217.125.52.181",st)
